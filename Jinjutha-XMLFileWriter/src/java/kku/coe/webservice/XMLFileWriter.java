/*
 * This program is call "XMLFileWriter" (DOM) that search quotes with the given authors.
 * search by keyword.txt that contain the authors name.
 * and quotes.xml is in folder project.
 * @author Jinjutha Teeranittayaparp 533040435-0
 */


package kku.coe.webservice;

 
import java.io.File;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
 
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
 
public class XMLFileWriter {
 
	public static void main(String argv[]) {
 
	  try {
 
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
 
		// root elements
		Document doc = docBuilder.newDocument();
		Element rootElement = doc.createElement("quotes");
		doc.appendChild(rootElement);
 
	
		Element quote = doc.createElement("quote");
		rootElement.appendChild(quote);
 

		Element words = doc.createElement("words");
		words.appendChild(doc.createTextNode("Time is more value than money. You can get more money, but you cannot get more time."));
		quote.appendChild(words);

		Element by = doc.createElement("by");
		by.appendChild(doc.createTextNode("Jim Rohn"));
		quote.appendChild(by);
 
		
		Element quote2 = doc.createElement("quote");
		rootElement.appendChild(quote2);
                
                
		Element words2 = doc.createElement("words");
		words2.appendChild(doc.createTextNode("เมื่อทำอะไรสำเร็จ แม้เป็นก้าวเล็กๆ ของตัวเอง ก็ควรรู้จักให้รางวัลตัวเองบ้าง"));
		quote2.appendChild(words2);

		Element by2 = doc.createElement("by");
		by2.appendChild(doc.createTextNode("ว. วชิรเมธี"));
		quote2.appendChild(by2);
                
  
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(doc);
                transformer.setOutputProperty(OutputKeys.INDENT, "yes"); //format charecter to read easier file.xml
		StreamResult result = new StreamResult(new File("quotes.xml"));
 
		transformer.transform(source, result);
 
		System.out.println("File saved!");

	  } catch (ParserConfigurationException pce) {
		pce.printStackTrace();
	  } catch (TransformerException tfe) {
		tfe.printStackTrace();
	  }
	}
}