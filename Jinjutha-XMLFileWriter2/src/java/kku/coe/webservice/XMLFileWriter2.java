/*
 * This program is call "XMLFileWriter2" (DOM) that it produces an XML file 
 * "quotes.xml" with characters and new lines 
 * file "quotes.xml" was saved in project folder
 * @author Jinjutha Teeranittayaparp 533040435-0
 */


package kku.coe.webservice;

import java.io.FileWriter;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamWriter;

public class XMLFileWriter2 {

    public static void main(String[] args) {
        try {
            String fileName = "quote.xml";
            XMLOutputFactory xof = XMLOutputFactory.newInstance();
            XMLStreamWriter xtw = null;
            xtw = xof.createXMLStreamWriter(new FileWriter(fileName));
            xtw.writeStartDocument("utf-8", "1.0");
            xtw.writeStartElement("quotes");
            xtw.writeStartElement("quote");
            xtw.writeStartElement("words");
            xtw.writeCharacters("Time is more value than money.You can get more money, but you cannot get more time.");
            xtw.writeEndElement();
            xtw.writeStartElement("by");
            xtw.writeCharacters("Jim Rohn");
            xtw.writeEndElement(); 
            xtw.writeEndElement();
            
             xtw.writeStartElement("quote");
            xtw.writeStartElement("words");
            xtw.writeCharacters("เมื่อทำอะไรสำเร็จ แม้เป็นก้าวเล็กๆ ของตัวเอง ก็ควรรู้จักให้รางวัลตัวเองบ้าง");
            xtw.writeEndElement();
            xtw.writeStartElement("by");
            xtw.writeCharacters("ว. วชิรเมธี");
            xtw.writeEndElement(); 
            xtw.writeEndElement();
            
           xtw.writeEndElement();
            
            xtw.writeEndDocument();
            xtw.flush();
            xtw.close();
        } catch (Exception ex) {
            System.err.println("Exception occurred while running writer samples");
        }
        System.out.println("Done");
    }
}