/*
 * This program is call "FeedWriterForm" (DOM) that has a form that accepts title, link, and description
 * and all information can recognized Thai or English langugae.
 * Input information into a feed. The latest information is written as the last item in a feed
 * @author Jinjutha Teeranittayaparp 533040435-0
 */

package kku.coe.webservice;

import java.io.*;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;


@WebServlet(name = "FeedWriterForm", urlPatterns = {"/FeedWriterForm"})
public class FeedWriterForm extends HttpServlet {

    String fileDirectory = "C:/Users/Skippy/Documents/NetBeansProjects/Jinjutha-FeedWriterForm/feed.xml";
    File file = new File(fileDirectory);

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
       
        request.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();

        try {
            if (file.exists()) {
               

                DocumentBuilderFactory builderFactory =
                        DocumentBuilderFactory.newInstance();
                DocumentBuilder docBuilder = builderFactory.newDocumentBuilder();  
                
                Document doc = docBuilder.parse(file);
                FeedWriterForm fw = new FeedWriterForm();
                String titleRef = request.getParameter("title");
                String linkRef = request.getParameter("link");
                String desRef = request.getParameter("description");
                String feed = fw.createNewRssTree(doc, titleRef, linkRef, desRef);
                out.print(feed);

            } else {
                DocumentBuilderFactory builderFactory =
                        DocumentBuilderFactory.newInstance();
                DocumentBuilder docBuilder = builderFactory.newDocumentBuilder();
                Document doc = docBuilder.newDocument();
                FeedWriterForm fw = new FeedWriterForm();
                String titleRef = request.getParameter("title");
                String linkRef = request.getParameter("link");
                String desRef = request.getParameter("description");
                String feed = fw.createRssTree(doc, titleRef, linkRef, desRef);
                out.print(feed);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public String createRssTree(Document doc, String titleRef, String linkRef, String desRef) throws Exception {

        Element rss = doc.createElement("rss");

        rss.setAttribute("version", "2.0");
        doc.appendChild(rss);

        Element channel = doc.createElement("channel");
        rss.appendChild(channel);

        Element title = doc.createElement("title");
        channel.appendChild(title);
        Text titleT = doc.createTextNode("Khon	Kean	University	Rss	Feed");
        title.appendChild(titleT);

        Element desc = doc.createElement("description");
        channel.appendChild(desc);
        Text descT = doc.createTextNode("Khon Kean University Information News	Rss Feed");
        desc.appendChild(descT);

        Element link = doc.createElement("link");
        channel.appendChild(link);
        Text linkT = doc.createTextNode("http://www.kku.ac.th");
        link.appendChild(linkT);

        Element lang = doc.createElement("lang");
        channel.appendChild(lang);
        Text langT = doc.createTextNode("en-th");
        lang.appendChild(langT);

        Element item = doc.createElement("item");
        channel.appendChild(item);

        Element iTitle = doc.createElement("title");
        item.appendChild(iTitle);
        Text iTitleT = doc.createTextNode(titleRef);
        iTitle.appendChild(iTitleT);
        Element iDesc = doc.createElement("description");
        item.appendChild(iDesc);
        Text iDescT = doc.createTextNode(desRef);
        iDesc.appendChild(iDescT);
        Element iLink = doc.createElement("link");
        item.appendChild(iLink);
        Text iLinkT = doc.createTextNode(linkRef);

        iLink.appendChild(iLinkT);
        Element pubDate = doc.createElement("pubDate");
        item.appendChild(pubDate);
        Text pubDateT = doc.createTextNode((new java.util.Date()).toString());
        pubDate.appendChild(pubDateT);

        TransformerFactory factory = TransformerFactory.newInstance();
        Transformer transformer = factory.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");

        StringWriter sw = new StringWriter();
        StreamResult result = new StreamResult(sw);
        DOMSource source = new DOMSource(doc);
        transformer.transform(source, result);
        String xmlString = sw.toString();


        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file),"UTF-8"));
        bw.write(xmlString);
        bw.flush();
        bw.close();
        return xmlString;
    }

    public String createNewRssTree(Document doc, String titleRef, String linkRef, String desRef) throws Exception {
    
      
        Node channel = doc.getElementsByTagName("channel").item(0);


        Element item = doc.createElement("item");
        channel.appendChild(item);

        Element iTitle = doc.createElement("title");
        item.appendChild(iTitle);
        Text iTitleT = doc.createTextNode(titleRef);
        iTitle.appendChild(iTitleT);
        Element iDesc = doc.createElement("description");
        item.appendChild(iDesc);
        Text iDescT = doc.createTextNode(desRef);
        iDesc.appendChild(iDescT);
        Element iLink = doc.createElement("link");
        item.appendChild(iLink);
        Text iLinkT = doc.createTextNode(linkRef);

        iLink.appendChild(iLinkT);
        Element pubDate = doc.createElement("pubDate");
        item.appendChild(pubDate);
        Text pubDateT = doc.createTextNode((new java.util.Date()).toString());
        pubDate.appendChild(pubDateT);

        TransformerFactory factory = TransformerFactory.newInstance();
        Transformer transformer = factory.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");

        StringWriter sw = new StringWriter();
        StreamResult result = new StreamResult(sw);
        DOMSource source = new DOMSource(doc);
        transformer.transform(source, result);
        String xmlString = sw.toString();


        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file),"UTF-8"));
        bw.write(xmlString);
        bw.flush();
        bw.close();
        return xmlString;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short	description";
    }//	</editor-fold>
}