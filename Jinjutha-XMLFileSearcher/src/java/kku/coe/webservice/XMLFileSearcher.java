/*
 * This program is call "XMLFileWriter" (DOM) that it produces an XML file 
 * "quotes.xml" with characters and new lines 
 * file "quotes.xml" was saved in project folder
 * @author Jinjutha Teeranittayaparp 533040435-0
 */


package kku.coe.webservice;

import java.io.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.*;
import org.w3c.dom.NodeList;

public class XMLFileSearcher {

    private static final String FILE_ENCODE = "UTF-8";

    public static void main(String[] args) {
        String data = null;
        BufferedReader dataIns = null;
        String file = "keyword.txt";
        String tempData;
        try {
            dataIns = new BufferedReader(new InputStreamReader( new FileInputStream(file), FILE_ENCODE));

            while ((data = dataIns.readLine()) != null) {
               //System.out.println("Line 1 :" + data);

                 tempData = data;
                try {

                    File fXmlFile = new File("quotes.xml");
                    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                    Document doc = dBuilder.parse(fXmlFile);
                    doc.getDocumentElement().normalize();

                    //System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
                    NodeList nList = doc.getElementsByTagName("quote");
                    //System.out.println("-----------------------");

                    for (int temp = 0; temp < nList.getLength(); temp++) {
                            
                            Element eElement = (Element) nList.item(temp);
                            
                            //System.out.print("word : " + getElemValue(eElement,"word"));
                            //System.out.println("by : " + getElemValue(eElement,"by")); 
                            if(getElemValue(eElement, "by").toLowerCase().contains(tempData.toLowerCase())){
                                System.out.println(getElemValue(eElement,"words") +" by "+ getElemValue(eElement,"by") );
                            }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            dataIns.close();
            dataIns = null;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String getTagValue(String by, Element eElement) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    private static String getElemValue(Element parent, String label) {
        
        Element e = (Element) parent.getElementsByTagName(label).item(0);
        try{
            Node child = e.getFirstChild();
            if(child instanceof CharacterData){
                CharacterData cd = (CharacterData) child;
                return cd.getData();
                
            }
        }catch(Exception ex){
            
        } return "";
    }
    
    
}
