/*
 * This program is call "FeedWriterForm2" (StAX) that has a form that accepts title, link, and description
 * and all information can recognized Thai or English langugae.
 * Input information into a feed. The latest information is written as the last item in a feed
 * @author Jinjutha Teeranittayaparp 533040435-0
 */

package kku.coe.webservice;

import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import javax.xml.stream.*;
import javax.xml.stream.events.*;
import javax.xml.transform.OutputKeys;

@WebServlet(name = "FeedWriterForm2", urlPatterns = {"/FeedWriterForm2"})
public class FeedWriterForm2 extends HttpServlet {

    // filePath is a location of feed file
    String fileDirectory = "C:/Users/Skippy/Documents/NetBeansProjects/Jinjutha-FeedWriterForm2/web/feed.xml";
    File file = new File(fileDirectory);

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // Set encoding of request data is UTF-8
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();

        try {
            // Get title, url and description from index.jsp
            String rssTitle = request.getParameter("title");
            String rssUrl = request.getParameter("link");
            String rssDescription = request.getParameter("description");

            if (file.exists()) {
                // If the feed file exits, the lastest filled information
                // is appended to the existing file
                new FeedWriterForm2().updateRssFeed(rssTitle, rssDescription, rssUrl);
            } else {
                // If the feed file does not exits, the programe creates
                // a new feed file
                new FeedWriterForm2().createRssFeed(rssTitle, rssDescription, rssUrl);
            }

            out.println("<b><a href='feed.xml'>Rss Feed</a> was create successfully</b>");

        } catch (Exception e) {
            System.out.println();
        }
    }

    private void createRssFeed(String rssTitle, String rssDescription, String rssUrl) throws Exception {
        XMLOutputFactory xof = XMLOutputFactory.newInstance();
        XMLStreamWriter xtw = xof.createXMLStreamWriter(new OutputStreamWriter(
                new FileOutputStream(file),"UTF-8"));

        xtw.writeStartDocument();
        xtw.writeStartElement("rss");
        xtw.writeAttribute("version", "2.0");
        xtw.writeStartElement("channel");
        xtw.writeStartElement("title");
        xtw.writeCharacters("Khon Kean University RSS Feed");
        xtw.writeEndElement();
        //end title
        xtw.writeStartElement("description");
        xtw.writeCharacters("Khon Kean University Information News RSS Feed");
        xtw.writeEndElement();
        //end description
        xtw.writeStartElement("link");
        xtw.writeCharacters("http://www.kku.ac.th");
        xtw.writeEndElement();
        //end link
        xtw.writeStartElement("lang");
        xtw.writeCharacters("en-th");
        xtw.writeEndElement();
        //end lang

        //element/rss/channel/item
        xtw.writeStartElement("item");
        //element/rss/channel/item/title
        xtw.writeStartElement("title");
        xtw.writeCharacters(rssTitle);
        xtw.writeEndElement();
        //element/rss/channel/item/description
        xtw.writeStartElement("description");
        xtw.writeCharacters(rssDescription);
        xtw.writeEndElement();
        //element/rss/channel/item/link
        xtw.writeStartElement("link");
        xtw.writeCharacters(rssUrl);
        xtw.writeEndElement();
        //element/rss/channel/item/pubDate
        xtw.writeStartElement("pubDate");
        xtw.writeCharacters((new java.util.Date()).toString());
        xtw.writeEndElement();
        xtw.writeEndElement();
        //end element item
        xtw.writeEndElement();
        //end element channel
        xtw.writeEndElement();
        //end element rss
        xtw.writeEndDocument();
        xtw.flush();
        xtw.close();
    }

    private void updateRssFeed(String rssTitle, String rssDescription, String rssUrl) throws Exception {
        XMLInputFactory xif = XMLInputFactory.newInstance();
        XMLEventReader feedReader = xif.createXMLEventReader(new InputStreamReader(new FileInputStream(file)));
        XMLOutputFactory xof = XMLOutputFactory.newInstance();
        XMLStreamWriter feedWriter = xof.createXMLStreamWriter(new OutputStreamWriter(new FileOutputStream(file),"UTF-8"));
        String elementName;

        while (feedReader.hasNext()) {
            XMLEvent event = feedReader.nextEvent();

            // Found start document
            if (event.getEventType() == XMLEvent.START_DOCUMENT) {
                feedWriter.writeStartDocument("UTF-8", "1.0");
            }

            // Found start element
            if (event.getEventType() == XMLEvent.START_ELEMENT) {
                StartElement element = (StartElement) event;
                elementName = element.getName().getLocalPart();
                if (elementName.equals("rss")) {
                    feedWriter.writeStartElement("rss");
                    feedWriter.writeAttribute("version", "2.0");
                } else {
                    feedWriter.writeStartElement(elementName);
                }
            }

            /*
             if (event.getEventType() == XMLEvent.ATTRIBUTE) {
             Attribute attribute = (Attribute) event;
             String attributeName = attribute.getName().getLocalPart();
             String attributeValue = attribute.getValue();
             feedWriter.writeAttribute(attributeName, attributeValue);
             }
             * */

            // Found end element
            if (event.isEndElement()) {
                EndElement element = (EndElement) event;
                elementName = element.getName().getLocalPart();
                // Appended item into channel when found element channel
                if (elementName.equals("channel")) {
                    feedWriter.writeStartElement("item");

                    feedWriter.writeStartElement("title");
                    feedWriter.writeCharacters(rssTitle);
                    feedWriter.writeEndElement();

                    feedWriter.writeStartElement("description");
                    feedWriter.writeCharacters(rssDescription);
                    feedWriter.writeEndElement();

                    feedWriter.writeStartElement("link");
                    feedWriter.writeCharacters(rssUrl);
                    feedWriter.writeEndElement();

                    feedWriter.writeStartElement("pubDate");
                    feedWriter.writeCharacters((new java.util.Date()).toString());
                    feedWriter.writeEndElement();
                    feedWriter.writeEndElement();
                    feedWriter.writeEndDocument();
                    feedWriter.flush();
                    feedWriter.close();
                    return;
                } else {
                    feedWriter.writeEndElement();
                }
            }
            
            // Found character
            if (event.isCharacters()) {
                Characters characters = (Characters) event;
                feedWriter.writeCharacters(characters.getData().toString());
            }
        }
      
        
        
        feedWriter.flush();
        feedWriter.close();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
          