/*
 * This program is call "XMLFileWriter2" (StAX) that search quotes with the given authors.
 * search by keyword.txt that contain the authors name.
 * and quotes.xml is in folder project.
 * @author Jinjutha Teeranittayaparp 533040435-0
 */
package kku.coe.webservice;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

public class XMLFileSearcher2 {

    public static void main(String args[]) throws FileNotFoundException, XMLStreamException, UnsupportedEncodingException, IOException {
        String eName;
        boolean quoteFound = false;
        boolean wordsFound = false;
        boolean byFound = false;
        String byText = null;
        String wordsText = null;

        String data = null;
        BufferedReader dataIns = null;
        String file = "keyword.txt";
        String tempData;

        XMLFileSearcher2 read = new XMLFileSearcher2();
        XMLInputFactory inputFactory = XMLInputFactory.newInstance();
        // Setup a new eventReader
        InputStream in = new FileInputStream("quotes.xml");
        XMLEventReader eventReader = inputFactory.createXMLEventReader(in);
        // Read the XML document




        while (eventReader.hasNext()) {
            XMLEvent event = eventReader.nextEvent();

            if (event.isStartElement()) {
                StartElement element = (StartElement) event;
                eName = element.getName().getLocalPart();

                if (eName.equals("quote")) {
                    quoteFound = true;
                }
                if (quoteFound && eName.equals("words")) {
                    wordsFound = true;
                }
                if (quoteFound && eName.equals("by")) {
                    byFound = true;
                }
            }
            if (event.isEndElement()) {
                EndElement element = (EndElement) event;
                eName = element.getName().getLocalPart();
                if (eName.equals("quote")) {
                    quoteFound = false;
                }
                if (quoteFound && eName.equals("words")) {
                    wordsFound = false;
                }
                if (quoteFound && eName.equals("by")) {
                    byFound = false;
                }
            }
            if (event.isCharacters()) {

                dataIns = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
                while ((data = dataIns.readLine()) != null) {
                    tempData = data;
                    //System.out.print(tempData + " ");
                   Characters characters = (Characters) event;
                    if (byFound) {
                        byText = characters.getData();
                        //System.out.print(byText);
                        if (byText.contains(tempData)) {
                          // System.out.print(wordsText);
                            System.out.println(wordsText + " by " + byText);
                        }
                    }
                    if (wordsFound) {
                        wordsText = characters.getData();
                    }

                }

            }
        }
    }
}
